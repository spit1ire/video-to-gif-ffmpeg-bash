#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

palette="/tmp/palette.png"
extension="*.mp4"
input=$(find $DIR -name "$extension") # only one video file in script directory
output="out.gif"

res="800" # horizontal resolution
ps="30" # the playback speed
fps="10" # discards frames (drop frames if less than ps)

ffmpeg -i $input -vf "scale=$res:-1:flags=lanczos,palettegen" -y $palette
ffmpeg -r $ps -i $input -i $palette -lavfi "scale=$res:-1:flags=lanczos [x]; [x][1:v] paletteuse" -r $fps -y $output